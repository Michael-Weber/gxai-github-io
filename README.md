# **[gxai.github.io](https://gxai.github.io)**
## **[Home](https://gxai.github.io)**  **[About](https://gxai.github.io/About)**
### **gxai - Artificial intelligence**
**Information intelligence interchange**

<a href="url"><img src="https://gxai.github.io/226192463_164229385778427_3497832883150811529_n.jpg" height="auto" width="400" style="border-radius:50%"></a>

### gxai- Founder - Michael Weber

## **[vvv.code.blog](https://vvv.code.blog){:target="_blank"}**
**Code For The Future**

Artificial intelligence interface

## **[Google Developer](https://www.meetup.com/en-AU/gdg-silicon-valley/members/336931816/){:target="_blank"}**
**GDG Silicon Valley - GDG London**

Google Developer Gruop

## **[Google My Business](https://vvv.code.blog/google-my-business/){:target="_blank"}**
**Google My Business Optimization**

Google account management

## **[Google Maps](https://maps.app.goo.gl/Lnubtwco1j3RKj568){:target="_blank"}**
**Level 8 - 1,1 Million Views**

Local Guides on Google Maps

## **[W3C](https://www.w3.org/community/aikr/wiki/User:Michaelweber){:target="_blank"}**
**The World Wide Web Consortium**

Artificial intelligence Knowledge representation

## **[GitHub](https://github.com/gxai){:target="_blank"}**
**gxai - Artificial intelligence**

Information intelligence interchange


## **[Page Speed](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fgxai.github.io%2F&tab=desktop){:target="_blank"}**
**Watch how fast this Website is!**

Google Developers PageSpeed Insights

### Visit also


### **[Local Guides Netherlands](https://m.facebook.com/Local-Guides-Netherlands-110067524667431#){:target="_blank"}**

### **[Amsterdam Photo Blog](https://amsterdam.photo.blog){:target="_blank"}**


### **Contact**

## **[Call gxai](tel:31649557828)**

## **[WhatsApp gxai](https://wa.me/message/VN3GO6SKBR4PH1)**

## **[E-Mail gxai](mailto:gxai.git@gmail.com)**

**[Legal](https://gxai.github.io/legal)**

Copyright © 2021 gxai
